import urllib.parse
from flask import request
def obtenir_query_string():
    page_voulue = urllib.parse.quote(request.endpoint)
    page_voulue = f'page_voulue={page_voulue}'
    if len(request.query_string) > 0:
        query = request.query_string.decode("utf-8", "ignore") 
        query = f'{query}&{page_voulue}'
    else:
        query = page_voulue
    return query