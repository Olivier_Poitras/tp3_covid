"""Application pour gerer les routes au depart de l'app."""
from flask import Flask, redirect, url_for, render_template, make_response
from blueprints.cas.gestion_cas import cas_routes
from blueprints.comptes.gestion_comptes import comptes_routes
import config
from db import db
from query_string import obtenir_query_string

# pylint: disable=W0613

def creer_application():
    """
    Fonction qui crée l'application selon la config.
    """
    app_flask = Flask(__name__)
    app_flask.config.from_object(config.DevelopmentConfig)
    app_flask.register_blueprint(cas_routes)
    app_flask.register_blueprint(comptes_routes)
    compte_bd = f'{app_flask.config["DB_USERNAME"]}:{app_flask.config["DB_PASSWORD"]}'
    serveur_bd = f'{app_flask.config["DB_SERVEUR"]}/{app_flask.config["DB_NAME"]}'
    app_flask.config['SQLALCHEMY_DATABASE_URI'] = f"""mysql://{compte_bd}@{serveur_bd}"""
    app_flask.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app_flask.config["BABEL_DEFAULT_LOCALE"] = "fr_CA"
    app_flask.config['SECRET_KEY'] = 'maCleSecrete'
    db.init_app(app_flask)
    return app_flask

app = creer_application()

@app.route("/")
def accueil():
    """La page d'accueil du site."""
    return redirect(url_for("cas.afficherRegions"))

@cas_routes.route("/fr")
def francais():
    """Changer la region pour avoir la langue en francais."""
    reponse = make_response(redirect('/'))
    reponse.set_cookie('langue', 'fr_CA', max_age=60*60*24*365)
    return reponse

@cas_routes.route("/en")
def anglais():
    """Changer la region pour avoir la langue en anglais."""
    reponse = make_response(redirect('/'))
    reponse.set_cookie('langue', 'en_CA', max_age=60*60*24*365)
    return reponse

@app.errorhandler(401)
def erreur_401(erreur):
    """Erreur 401."""
    query = obtenir_query_string()
    return render_template("401.html", message="Vous devez vous authentifier", query=query), 401

@app.errorhandler(403)
def erreur_403(erreur):
    """Erreur 403."""
    return render_template("403.html"), 403

if __name__ == "__main__":
    app.run(debug=app.config["DEBUG"])
