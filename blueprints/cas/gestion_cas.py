"""Gestion des cas et regions de covid."""
# pylint: disable=E0401
# pylint: disable=C0103
# pylint: disable=W0622
from datetime import datetime
from flask import (
    Blueprint,
    render_template,
    request,
    redirect,
    url_for,
    session,
    abort,
    flash,
    make_response
)
from babel import (
    numbers,
    dates
)
from validateurs_formulaires.creation_cas import CreationFormulaire
from models.cas import Cas
from models.region import Regions
from db import db

cas_routes = Blueprint('cas', __name__, url_prefix='/cas', template_folder='templates')

def get_locale():
    """Fonction qui retourne la locale de Babel.

    Returns:
        Retourne la configuration locale de Babel.
    """
    return "fr_CA"

@cas_routes.route("/fr")
def francais():
    """Mettre la langue en francais."""
    reponse = make_response(redirect('/'))
    reponse.set_cookie('langue', 'fr_CA', max_age=60*60*24*365)
    return reponse

@cas_routes.route("/en")
def anglais():
    """Mettre la langue en anglais."""
    reponse = make_response(redirect('/'))
    reponse.set_cookie('langue', 'en_CA', max_age=60*60*24*365)
    return reponse

@cas_routes.route("/")
def accueil():
    """Accueil"""
    return redirect(url_for("cas.afficher"))

@cas_routes.route('/cas')
def afficher():
    """Affichage des cas de covid."""
    compte_id = session.get("compte_id")
    if not compte_id:
        estConnecte = False
        session['url'] = url_for('cas.afficher')
        abort(401)
    else:
        estConnecte = True
    if not session.get("est_admin"):
        estAdmin = False
    else:
        estAdmin = True
    cas = Cas.query.all()
    les_initiales = []
    les_dates = []
    if "langue" in request.cookies:
        langue = request.cookies.get("langue")
    else:
        langue = get_locale()
    for un_cas in cas:
        les_dates.append(dates.format_date(un_cas.date, locale=langue))
        initiales_pour_cas = ""
        le_nom_complet = ""
        le_nom_complet += un_cas.prenom
        le_nom_complet += " "
        le_nom_complet += un_cas.nom
        tempo = (le_nom_complet)
        liste_nom_complet = tempo.split()
        for name in liste_nom_complet:
            initiales_pour_cas += name[0].upper()
        les_initiales.append(initiales_pour_cas)
    date = datetime.today()
    date = dates.format_date(date, locale=langue)
    return render_template('liste_admin.html', cas=cas, les_initiales=les_initiales,\
         estConnecte=estConnecte, date=date, estAdmin=estAdmin, dates=les_dates)

@cas_routes.route('/regions')
def afficherRegions():
    """Affichage des cas par regions."""
    compte_id = session.get("compte_id")
    if not compte_id:
        estConnecte = False
    else:
        estConnecte = True
    regions = Regions.query.all()
    if "langue" in request.cookies:
        langue = request.cookies.get("langue")
    else:
        langue = get_locale()
    date = datetime.today()
    date = dates.format_date(date, locale=langue)
    return render_template('liste_regions.html', regions=regions, date=date,\
         number=numbers.format_number, langue=langue, estConnecte=estConnecte)

@cas_routes.route('/<int:id>/suppression')
def supprimer(id):
    """Supprimer un cas si l'utilisateur est admin."""
    if not session.get("compte_id"):
        abort(401)
    if not session.get("est_admin"):
        abort(403)
    Cas.query.filter_by(id=id).delete()
    db.session.commit()
    flash('La suppression a été effectuée avec succès.')
    return redirect(url_for("cas.afficher"))

@cas_routes.route('/saisie', methods=['GET', 'POST'])
def saisir():
    """Fonction qui gere la creation de cas."""
    compte_id = session.get("compte_id")
    if not compte_id:
        estConnecte = False
        session['url'] = url_for('cas.saisir')
        abort(401)
    else:
        estConnecte = True
    if request.method == "POST":
        nom = request.form.get("nom")
        prenom = request.form.get("prenom")
        region_id = request.form.get("region")
        validateur = CreationFormulaire(nom, prenom, region_id)
        messages = validateur.valider()
        if len(messages) > 0:
            return render_template("saisie.html", messages=messages, nom=nom, prenom=prenom, \
                region_id=region_id, regions=Regions.query.order_by(Regions.nom).all())
        cas = Cas(date = datetime.today().strftime('%Y-%m-%d'),\
             nom = nom, prenom = prenom, region_id = region_id, compte_id = compte_id)
        db.session.add(cas)
        db.session.commit()
        return render_template("succes.html", estConnecte=True)
    if "langue" in request.cookies:
        langue = request.cookies.get("langue")
    else:
        langue = get_locale()
    date = datetime.today()
    date = dates.format_date(date, locale=langue)
    return render_template("saisie.html", regions=Regions.\
        query.order_by(Regions.nom).all(), date=date, estConnecte=estConnecte)
