"""Gestion des comptes."""
# pylint: disable=E0401
# pylint: disable=C0103
# pylint: disable=W0622
# pylint: disable=W0703
# pylint: disable=R1710
# pylint: disable=W0621
import hashlib
import urllib.parse
from flask import (
    Blueprint,
    render_template,
    request,
    redirect,
    url_for,
    session,
    make_response,
    abort
)
from mysql import connector
from models.compte import Comptes
from db import db

comptes_routes = Blueprint('comptes', __name__, url_prefix='/comptes', template_folder='templates')

def obtenir_connexion():
    """Connexion a la base de donnee sql."""
    try:
        conn = connector.connect(
              user="root",
              password="",
              host="127.0.0.1",
              port=3306,
              database="tp3_covid")
        return conn
    except Exception as e:
        print(e)

@comptes_routes.route("/comptes")
def afficher():
    compte_id = session.get("compte_id")
    if not compte_id:
        estConnecte = False
        session['url'] = url_for('cas.saisir')
        abort(401)
    else:
        estConnecte = True
    if not session.get("est_admin"):
        estAdmin = False
        abort(403)
    else:
        estAdmin = True
    return render_template('comptes.html', estConnecte=estConnecte, estAdmin=estAdmin, comptes=Comptes.query.order_by(Comptes.compte).all())

@comptes_routes.route("/connexion")
def connexion():
    """La connexion suivant l'erreur."""
    query = request.query_string.decode("utf-8", "ignore")
    return render_template('connexion.html', query=query)

@comptes_routes.route('/connexion', methods=["POST"])
def connexion_post():
    """Fonction qui gere la connexion."""
    compte = request.form.get("compte", "")
    mot_de_passe = request.form.get("mot_de_passe", "")
    if not compte or not mot_de_passe:
        return render_template('connexion.html',\
                message="Information incorrecte", compte=compte, mot_de_passe=mot_de_passe)
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        mot_de_passe = hashlib.sha256(mot_de_passe.encode()).hexdigest()
        curseur.execute("SELECT admin FROM comptes WHERE compte = %s and\
             mot_passe = %s", (compte, mot_de_passe))
        reponse = curseur.fetchone()
        if reponse is None:
            return render_template('connexion.html', \
                message="compte introuvable ou mauvais mot de passe",\
                     compte=compte, mot_de_passe=mot_de_passe)
        admin, = reponse
        session["compte_id"] = Comptes.query.filter_by(compte=compte).first().id
        session["est_admin"] = admin == 1
        page_voulue = request.args.get("page_voulue")
        if page_voulue:
            page_voulue = urllib.parse.unquote(page_voulue)
            reponse = make_response(redirect(url_for(page_voulue)))
            reponse.status_code = 303
            return reponse
        if 'url' in session:
            return redirect(session['url'])
        if admin == 1:
            return redirect(url_for("cas.afficher")), 303
        return redirect(url_for("cas.afficherRegions")), 303
    finally:
        connexion.close()

@comptes_routes.route('/inscription', methods=['GET', 'POST'])
def inscription():
    """Fonction qui gere l'inscription."""
    if request.method == "POST":
        identifiant = request.form.get("identifiant")
        mdp = request.form.get("mdp")
        mdp = hashlib.sha256(mdp.encode()).hexdigest()
        if request.form.get("admin"):
            admin = 1
        else:
            admin = 0
        compte = Comptes(compte = identifiant, mot_passe = mdp, admin = admin)
        db.session.add(compte)
        db.session.commit()
        return redirect(url_for("comptes.connexion"))
    return render_template('inscription.html')

@comptes_routes.route('/deconnexion')
def deconnexion():
    """Fonction qui gere la deconnexion."""
    session.clear()
    return redirect(url_for("comptes.connexion"))
