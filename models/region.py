from db import db

class Regions(db.Model):
    __tablename__ = 'regions'
    id = db.Column(db.Integer, primary_key=True)
    numero_region = db.Column(db.String(2), unique=True, nullable=False)
    nom = db.Column(db.String(80), unique=True, nullable=False)

    def __repr__(self):
        return '<Region %r>' % self.nom