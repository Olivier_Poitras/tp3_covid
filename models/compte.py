from db import db

class Comptes(db.Model):
    __tablename__ = 'comptes'
    id = db.Column(db.Integer, primary_key=True)
    compte = db.Column(db.String(45), unique=True, nullable=False)
    mot_passe = db.Column(db.String(100), nullable=False)
    admin = db.Column(db.Boolean, default=0)

    def __repr__(self):
        return '<Compte %r>' % self.compte