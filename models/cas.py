from db import db

class Cas(db.Model):
    __tablename__ = 'cas'
    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.Date, nullable=True)
    nom = db.Column(db.String(45))
    prenom = db.Column(db.String(45))
    region_id = db.Column(db.Integer, db.ForeignKey('regions.id'), nullable=False)
    region = db.relationship('Regions',
        backref=db.backref('cas', lazy=True))
    compte_id = db.Column(db.Integer, db.ForeignKey('comptes.id'), nullable=False)
    compte = db.relationship('Comptes',
        backref=db.backref('cas', lazy=True))

    def __repr__(self):
        return '<Cas %r>' % self.nom
