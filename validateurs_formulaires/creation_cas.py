from models.region import Regions

class CreationFormulaire():
    messages={}
    def __init__(self, nom, prenom, region):
        self.nom = nom
        self.prenom = prenom
        self.region = region
    
    def valider(self):
        if self.nom is None or self.nom == "":
            self.messages["nom"] = "Le nom est obligatoire"
        elif len(self.nom) > 45:
            self.messages["nom"] = "Le nom ne doit pas avoir plus de 45 caractères"
        if self.prenom is None or self.prenom == "":
            self.messages["prenom"] = "Le prénom est obligatoire"
        elif len(self.prenom) > 45:
            self.messages["prenom"] = "Le prénom ne doit pas avoir plus de 45 caractères"
        if self.region is None or self.region == "":
            self.messages["region"] = "La région est requise"
        else:
            region = Regions.query.get(self.region)
            if region is None:
                self.messages["region"] = "La région est requise et doit être l'une des régions de la liste"
        return self.messages