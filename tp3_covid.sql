-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Nov 09, 2021 at 04:55 PM
-- Server version: 5.7.31
-- PHP Version: 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tp3_covid`
--

-- --------------------------------------------------------

--
-- Table structure for table `cas`
--

DROP TABLE IF EXISTS `cas`;
CREATE TABLE IF NOT EXISTS `cas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` date DEFAULT NULL,
  `prenom` varchar(45) DEFAULT NULL,
  `nom` varchar(45) DEFAULT NULL,
  `region_id` int(11) NOT NULL,
  `compte_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_region_idx` (`region_id`),
  KEY `compte_id` (`compte_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `comptes`
--

DROP TABLE IF EXISTS `comptes`;
CREATE TABLE IF NOT EXISTS `comptes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `compte` varchar(45) NOT NULL,
  `mot_passe` varchar(100) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comptes`
--

INSERT INTO `comptes` (`id`, `compte`, `mot_passe`, `admin`) VALUES
(1, 'utilisateur_normal', 'f947d7e4388041e18f13fc9a220c8265316e2f60b376e709d6c5e7b269d4b221', 0),
(2, 'utilisateur_admin', 'f947d7e4388041e18f13fc9a220c8265316e2f60b376e709d6c5e7b269d4b221', 1);

-- --------------------------------------------------------

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
CREATE TABLE IF NOT EXISTS `regions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `numero_region` char(2) NOT NULL,
  `nom` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regions`
--

INSERT INTO `regions` (`id`, `numero_region`, `nom`) VALUES
(1, '01', 'Bas-Saint-Laurent'),
(2, '02', 'Saguenay–Lac-Saint-Jean'),
(3, '03', 'Capitale-Nationale'),
(4, '05', 'Estrie'),
(5, '06', 'Montréal'),
(6, '07', 'Outaouais'),
(7, '08', 'Abitibi-Témiscamingue'),
(8, '09', 'Côte-Nord'),
(9, '10', 'Nord-du-Québec'),
(10, '11', 'Gaspésie–Îles-de-la-Madelein'),
(11, '12', 'Chaudière-Appalaches'),
(12, '13', 'Laval'),
(13, '14', 'Lanaudière'),
(14, '15', 'Laurentides'),
(15, '16', 'Montérégie'),
(16, '17', 'Centre-du-Québec'),
(17, '04', 'Mauricie');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `cas`
--
ALTER TABLE `cas`
  ADD CONSTRAINT `FK_region` FOREIGN KEY (`region_id`) REFERENCES `regions` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `cas_ibfk_1` FOREIGN KEY (`compte_id`) REFERENCES `comptes` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
