class Config(object):
    DEBUG = False
    TESTING = False
    SECRET_KEY = "B\xb2?.\xdf\x9f\xa7m\xf8\x8a%,\xf7\xc4\xfa\x91"

    DB_SERVEUR = "localhost"
    DB_NAME = "travail_3_prod"
    DB_USERNAME = "admin"
    DB_PASSWORD = ""

    SESSION_COOKIE_SECURE = True

class ProductionConfig(Config):
    pass

class DevelopmentConfig(Config):
    DEBUG = True

    DB_NAME = "tp3_covid"
    DB_USERNAME = "root"
    DB_PASSWORD = ""
    
class TestingConfig(Config):
    Testing = True

    DB_NAME = "OlivierPoitras$tp3_covid"
    DB_USERNAME = "OlivierPoitras"
    DB_PASSWORD = "pwdTp3Covid"
    DB_SERVEUR = "OlivierPoitras.mysql.pythonanywhere-services.com"

    